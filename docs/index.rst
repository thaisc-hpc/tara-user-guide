.. ThaiSC Tara User Guide documentation master file, created by
   sphinx-quickstart on Fri Nov 30 14:17:10 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. only:: html

  |github badge|_ |doc badge|_ 
  
  .. .. |star badge|_

  .. |github badge| image:: https://img.shields.io/badge/sources-gitlab-green.svg 
  .. |doc badge| image:: https://readthedocs.org/projects/tara-admin/badge/?version=latest
  .. |star badge| image:: https://img.shields.io/github/stars/puttsk/nstda-sc.svg?style=social&label=Star
  .. _github badge: https://gitlab.com/thai-sc/tara-admin-documentation
  .. _doc badge: https://tara-admin.readthedocs.io/en/latest/
  .. _star badge: https://github.com/puttsk/thaisc

================
Tara User Guide 
================

.. toctree::
    :maxdepth: 2
    :caption: Contents
    :titlesonly:

    guides/overview.rst
    guides/access.rst
    guides/tara.rst
    guides/file.rst
    guides/applications.rst
    guides/compilation.rst
    guides/running-jobs.rst
    guides/faq.rst    
    tutorials/index.rst
    contribute.rst

.. only:: html

    Contributing
    ============

    See. :doc:`Contributing </contribute>`

    Authors
    =======

    * **Putt Sakdhnagool** - *Initial work* 

    See also the list of contributors_ who participated in this project.

    Issues / Feature request
    ========================

    You can submit bug / issues / feature request using Tracker_.

    License
    =======

    TBD

.. _contributors: https://github.com/puttsk/nstda-sc/graphs/contributors
.. _Tracker: https://github.com/puttsk/nstda-sc/issues
