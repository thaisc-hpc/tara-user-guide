============
Using Tara 
============

.. only:: html

    .. sidebar:: Page Contents

        .. contents:: 
            :local:

Tara nodes run CentOS 7 Linux. You will need some basic Linux knowledge 
and skills to use the system properly. 