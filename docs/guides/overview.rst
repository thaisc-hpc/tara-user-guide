=================
System Overview
=================

.. only:: html

    .. sidebar:: Page Contents

        .. contents:: 
            :local:

Introduction
===============

Tara is the first supercomputer at Thailand Supercomputer Center (ThaiSC).

Access to Tara requires ThaiSC account, which could be requested through this `form <http://test>`_.

System Configuration
========================

Tara nodes run CentOS 7 and use `Slurm Workload Manager <https://slurm.schedmd.com/overview.html>`_  
for resource and job management. 

Operating system and applications will be patched as security needs dictate.

Computational Nodes
---------------------

Tara cluster consists of three types of computational nodes: 

* Compute nodes (**tara-c**) 
* Memory-intensive nodes (**tara-m**) 
* GPU nodes (**tara-g**) 

Each nodes includes Intel Xeon Skylake CPUs, DDR4-2666 memory, and Infiniband EDR interconnect. 
Detailed hardware specification of each computational node is shown in :numref:`tara-node-spec`.
All computational nodes allow for unlimited stack usage, as well as unlimited core dump size 
(though disk space and disk quotas may still be a limiting factor).


Specialized Node
-----------------

Tara has three DGX-1 systems (**tara-dgx1**) which is specialy designed for AI and GPU-intensive workload. 
Each node has two Intel Xeon Broadwell CPU, 512GB DDR4-2333 memory, Infiniband EDR interconnect, 
and eight Nvidia Tesla V100 with 32GB device memory as shown in :numref:`tara-node-spec`. 

.. tabularcolumns:: |l|p{1.2cm}|p{7cm}|r|l|

.. csv-table:: Tara Node Specification
    :header: "Node", "Count", "Processor", "Memory", "GPU"
    :name: tara-node-spec
    :widths: auto

    "tara-c",       60, "2 Xeon Gold 6148 (20c @2.4GHz, Skylake)",      "192GB",    "\-"                
    "tara-m",       10, "8 Xeon Platinum 8160 (24c @2.4GHz, Skylake)",  "3072GB",   "\-"           
    "tara-g",       2,  "2 Xeon Gold 6148 (20c @2.4GHz, Skylake)",      "384GB",    "2 Tesla V100 16GB"
    "tara-dgx1",    3,  "2 Xeon E5-2698 v4(20c @2.2GHz, Broadwell)",    "512GB",    "8 Tesla V100 32GB"

Frontend Node
---------------

Users access Tara system through frontend nodes (**tara-frontend**), which has two Xeon Gold 6148 and 192GB DDR4-2666 memory. 
Currently, Tara has only one frontend node. Additional frontend nodes will be added to the system in the future phase. 

Network 
---------

The interconnect is a 100Gb/sec Infiniband EDR network with a fat tree topology.

File Systems
===============

Tara mounts two GPFS file systems, ``$HOME`` and ``$SCRATCH``, that are shared across all nodes. 

.. csv-table:: Tara File Systems
    :header: "File Systems", "Quota", "Overall Capacity", "Remarks"
    :name: tara-storage-spec
    :widths: 20, 20, 20, 40

    ``$HOME``,  "5GB, 100,000 files", "~100TB", "Home directory"
    ``$SCRATCH``, "No quota", "~80TB", "**Files are subject to purge**, if there is no access for more than 10 days."
